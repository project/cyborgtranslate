<!-- writeme -->
Cyborgtranslate
===============

Trigger Google translate and Drupal interface translation at the same time, allowing sections of your site's localization to be done by humans and the rest by machines.

 * https://www.drupal.org/project/cyborgtranslate
 * Issues: https://www.drupal.org/project/issues/cyborgtranslate
 * Source code: https://gitlab.com/agaric/drupal/cyborgtranslate/tree/8.x-1.x
 * Keywords: drupal, translation, multilingual, localization, google translate
 * Package name: drupal/cyborgtranslate


### Requirements

No dependencies.


### License

GPL-2.0+

<!-- endwriteme -->


### Background

Originally developed for the [Find It Program Locator platform](https://gitlab.com/find-it-program-locator ).



https://agaric.gitlab.io/raw-notes/notes/2019-11-06-pair-machine-translation-with-human-vetted-translation-in-a-way-that-s-transparent-to-the-viewer/
