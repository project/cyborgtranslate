/**
 * @file
 * File disclaimer.js.
 */

(function ($, Drupal, cookies) {
  'use strict';

  Drupal.behaviors.cyborgtranslateDisclaimer = {

    attach: function (context, settings) {
      var config = settings.cyborgtranslateDisclaimer || {};
      var disclaimerLink = $('.cyborgtranslate-dropdown', context);
      if (!disclaimerLink.length) {
        // We won't be able to do anything without the dropdown we hang the disclaimer on.
        return;
      }

      // When the user has previously activated google translate, the cookie
      // will be set and we can proceed straight to exposing the language
      // button without the disclaimer interstitial.
      var cyborgtranslate_disclaimer = cookies.get('cyborgtranslate_disclaimer');
      if (cyborgtranslate_disclaimer == 'accepted') {
        // Continue on our merry way and don't get in anyone's way.
      }
      else {
        // Listen for user click on the translate interstitial (disclaimer) link.
        disclaimerLink.click(function disclaimerListener(event) {
          event.preventDefault();

          // Show the disclaimer text if available.
          if (config.disclaimer &&
              config.disclaimer.trim().length > 0) {

            var disclaimerModal = $('<div class="message">' + config.disclaimer + '<div>').appendTo('body');
            Drupal.dialog(disclaimerModal, {
              title: config.disclaimerTitle,
              classes: {
                'ui-dialog': 'cyborgtranslate-disclaimer-modal',
              },
              width: 'auto',
              buttons: [
                {
                  text: config.acceptText,
                  click: function() {
                    $('#cyborgtranslate-btn-checkbox').prop('checked', true);
                    cookies.set('cyborgtranslate_disclaimer', 'accepted');
                    // If we don't stop listening, the dialog gets triggered every time until page reload.
                    disclaimerLink.off();
                    // When we move away from JQuery finally and disclaimerLink is not a JQuery object above'd be:
                    // disclaimerLink.removeEventListener('click', disclaimerListener);
                    $(this).dialog('close');
                  }
                },
                {
                  text: config.dontAcceptText,
                  click: function() {
                    $(this).dialog('close');
                  }
                }
              ]
            }).showModal();
          }

          // If the disclaimer text is not available, then just show the gadget.
          else {
            $('#cyborgtranslate-btn-checkbox').prop('checked', true);
          }
        });
      }
    }

  }
})(jQuery, Drupal, window.Cookies);
