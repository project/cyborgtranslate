/**
 * @file
 * File switcher.js.
 */

(function (Drupal) {
  'use strict';
  Drupal.behaviors.cyborgtranslateSwitcher = {
    attach: function () {
      // Close the dropdown if the user clicks outside of it.
      document.addEventListener('click', function(event) {
        // If we define the parent as a variable outside the event listener,
        // it stays null exactly when we need it, so might as well do it inline
        if (!event.composedPath().includes(document.querySelector('.cyborgtranslate-dropdown'))
          && document.getElementById('cyborgtranslate-btn-checkbox').checked) {
            document.getElementById('cyborgtranslate-btn-checkbox').checked = false;
        }
      });
    }
  }
})(Drupal);
