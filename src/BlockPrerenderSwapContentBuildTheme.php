<?php

namespace Drupal\cyborgtranslate;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Render\Element\RenderCallbackInterface;

/**
 * Provides a trusted callback to alter the commerce cart block.
 *
 * @see cyborgtranslate_block_view_alter().
 */
class BlockPrerenderSwapContentBuildTheme implements RenderCallbackInterface {

  /**
   * Sets
   */
  public static function preRender($build) {
    $build['content']['#theme'] = 'cyborgtranslate_links__language_block';
    /* @var \Drupal\Core\Config\ImmutableConfig $config */
    $config = \Drupal::service('config.factory')->get('cyborgtranslate.settings');
    $title = Xss::filterAdmin($config->get('disclaimer_title', ''));
    $text = Xss::filterAdmin($config->get('disclaimer', ''));
    if (!$text) {
      return;
    }
    $build['#attached']['library'][] = 'cyborgtranslate/disclaimer';
    $build['#attached']['drupalSettings']['cyborgtranslateDisclaimer'] = [
      'disclaimerTitle' => $title,
      'disclaimer' => $text,
      'acceptText' => t('Accept'),
      'dontAcceptText' => t('Do Not Accept'),
    ];
    return $build;
  }

}
