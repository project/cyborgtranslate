<?php

namespace Drupal\cyborgtranslate\Form;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure the Cyborg Translate settings.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * The Cache Render.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cacheRender;

  /**
   * {@inheritdoc}
   */
  public function __construct(ConfigFactoryInterface $config_factory, CacheBackendInterface $cache_render) {
    parent::__construct($config_factory);
    $this->cacheRender = $cache_render;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('cache.render')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'cyborgtranslate_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    $config = $this->config('cyborgtranslate.settings');

    $form['disclaimer_title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Service disclaimer title'),
      '#default_value' => $config->get('disclaimer_title'),
      '#size' => 40,
      '#description' => $this->t('Title for the modal. Only shows up if you also set text.'),
    ];

    $form['disclaimer'] = [
      '#title' => $this
        ->t('Service disclaimer text'),
      '#type' => 'textarea',
      '#default_value' => $config
        ->get('disclaimer'),
      '#description' => $this
        ->t('Optionally require users to accept a disclaimer (in a popup modal on click) before allowing translation. Allowed tags: @tags', [
          '@tags' => implode(', ', Xss::getAdminTagList()),
        ]),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('cyborgtranslate.settings');

    $config->set('disclaimer_title', $form_state->getValue('disclaimer_title'));
    $config->set('disclaimer', $form_state->getValue('disclaimer'));

    $config->save();

    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'cyborgtranslate.settings',
    ];
  }

}
